<?php

use App\Http\Controllers\Api\PageController;
use App\Http\Controllers\Api\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// public api
Route::get('/home', [PageController::class, 'index']);
Route::get('/about', [PageController::class, 'abuot']);
Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);




//private api
Route::group(['middleware' => 'auth:sanctum'], function() {
    Route::post('/store', [PageController::class, 'store']);
    Route::post('/logout', [AuthController::class, 'logout']); 
});


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
